import nltk
from nltk.chat.util import Chat, reflections

import speech_recognition as sr 
import pyaudio
from gtts import gTTS
import os
from openai import OpenAI
import pygame

client = OpenAI()

def play_mp3(file_path):
    pygame.init()
    pygame.mixer.init()

    try:
        pygame.mixer.music.load(file_path)
        # print(f"Now playing: {file_path}")
        pygame.mixer.music.play()

        # รอจนกว่าเสียงจะเล่นจบ
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)
    except Exception as e:
        print(f"Error: {e}")
    finally:
        pygame.mixer.quit()
        pygame.quit()

list_text = []
def talk():
        recog = sr.Recognizer()
        with sr.Microphone() as source:
            while True:
                audio = recog.listen(source)
                try:
                    text = recog.recognize_google(audio, language='en')
                    # print("You said: " + text)
                    break
                except sr.UnknownValueError:
                    print("Sorry, I could not understand what you said.")
        list_text.append({"role": "user", "content":text })
        print("User said: ",text)

        response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=list_text,
        n=1,
        max_tokens=700,
        temperature=0.6,
        presence_penalty=0.6,        
        
        )

        for i in response.choices:
            print("Assistance: ",i.message.content)

        mytext = response.choices[0].message.content
        list_text.append({"role": "system", "content":mytext})
        audio = gTTS(text=mytext, lang="en", slow=False)

        audio.save("example.mp3")
        play_mp3("example.mp3")