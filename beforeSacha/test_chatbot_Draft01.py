import nltk
from nltk.chat.util import Chat, reflections

import speech_recognition as sr 
import pyaudio
from gtts import gTTS
import os

sr.Microphone.list_microphone_names()
mic = sr.Microphone(9)
recog = sr.Recognizer()


# Define pairs of patterns and responses
pairs = [
    ["hi|hello|hey", ["Hello!", "Hi there!", "How can I help you?"]],
    ["what is your name?", ["I am a chatbot.", "You can call me ChatGPT."]],
    ["how are you?", ["I'm doing well, thank you!", "I'm fine. How about you?"]],
    ["quit", ["Bye!", "Goodbye!", "See you later!"]],
]

# Create a Chat object
chatbot = Chat(pairs, reflections)

# Interaction loop
print("Hello! I'm your chatbot. Type 'quit' to exit.")

with sr.Microphone() as source:
    while True:
        # print("you :")
        audio = recog.listen(source)
        try:
            text = recog.recognize_google(audio, language='en')
            print("You said: " + text)
        except sr.UnknownValueError:
            print("Sorry, I could not understand what you said.")
        response = chatbot.respond(text)
        print("ChatGPT:", response)

        mytext = response
        audio = gTTS(text=mytext, lang="en", slow=False)

        audio.save("example.mp3")
        os.system("start example.mp3")

        if text.lower() == 'quit':
            break




