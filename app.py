from flask import Flask, render_template, jsonify , request
# from Sacha import talk
import Sacha


app = Flask(__name__)
# api = Api(app)


@app.route('/',methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        print("-----------------say someting---------------------")
        Sacha.talk()
        print("--------------------------------------------------")
    return render_template('home.html')

# @app.route('/start_talking', methods=['GET'])
# def start_talking():
#     talk()  # Call the talk function from Sacha.py
#     return jsonify({'message': 'Talking started'})  # Return a response (optional)

if __name__ == '__main__':
    app.run(debug=True,port=5000)